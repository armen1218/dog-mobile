import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mydog/constants/constants.dart';
import 'package:mydog/utils/scale.dart';
import 'package:mydog/ui/widgets/custom_textfield.dart';
import 'package:mydog/ui/widgets/custom_spacer.dart';
import 'package:http/http.dart' as http;

const String AUTH0_DOMAIN = 'finaxar-staging.auth0.com';
const String AUTH0_CLIENT_ID = '8t8FfRIjp8bDSCMK77VkPQ4gCj4PBGcs';

const String AUTH0_REDIRECT_URI = 'mydognow.co://login-callback';
const String AUTH0_ISSUER = 'https://$AUTH0_DOMAIN';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  SignInScreenState createState() => SignInScreenState();
}

class SignInScreenState extends State<SignInScreen> {
  hScale(double scale) {
    return Scale().hScale(context, scale);
  }

  wScale(double scale) {
    return Scale().wScale(context, scale);
  }

  fSize(double size) {
    return Scale().fSize(context, size);
  }

  final emailCtr = TextEditingController();
  final passwordCtr = TextEditingController();

  bool flagRemember = false;

  handleLogin() async {
    // POST https://YOUR_DOMAIN/oauth/token
    // Content-Type: application/json
    // {
    //   "grant_type" : "http://auth0.com/oauth/grant-type/passwordless/otp",
    //   "client_id": "YOUR_CLIENT_ID",
    //   "client_secret": "YOUR_CLIENT_SECRET", // for web applications
    //   "otp": "CODE",
    //   "realm": "email|sms" //email or sms
    //   "username":"USER_EMAIL|USER_PHONE_NUMBER", // depends on which realm you chose
    //   "audience" : "API_IDENTIFIER", // in case you need an access token for a specific API
    //   "scope": "SCOPE"
    // }
    
    Navigator.of(context).pushReplacementNamed(SIGN_IN_AUTH);
  }

  handleRegister() {
    // POST https://YOUR_DOMAIN/dbconnections/signup
    // Content-Type: application/json
    // {
    // "client_id": "YOUR_CLIENT_ID",
    // "email": "EMAIL",
    // "password": "PASSWORD",
    // "connection": "CONNECTION",
    // "username": "johndoe",
    // "given_name": "John",
    // "family_name": "Doe",
    // "name": "John Doe",
    // "nickname": "johnny",
    // "picture": "http://example.org/jdoe.png"
    // "user_metadata": { plan: 'silver', team_id: 'a111' }
    // }
    
    Navigator.of(context).pushReplacementNamed(SIGN_UP);
  }

  handleForgotPwd() {
    Navigator.of(context).pushReplacementNamed(FORGOT_PWD);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        child: Scaffold(
            body: SingleChildScrollView(
                child: Column(children: [
      logo(),
      const CustomSpacer(size: 66),
      title(),
      const CustomSpacer(size: 50),
      CustomTextField(
          ctl: emailCtr, hint: 'Enter Email Address', label: 'Email'),
      const CustomSpacer(size: 32),
      CustomTextField(
          ctl: passwordCtr,
          hint: 'Enter Password',
          label: 'Password',
          pwd: true),
      const CustomSpacer(size: 24),
      forgotPwdField(),
      const CustomSpacer(size: 25),
      loginButton(),
      const CustomSpacer(size: 62),
      registerField()
    ]))));
  }

  Widget logo() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: hScale(265),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/signin_top_background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                'assets/logo.png',
                fit: BoxFit.contain,
                width: wScale(137),
              )
            ]));
  }

  Widget title() {
    return Text("Let’s Sign You In",
        style: TextStyle(
            color: const Color(0xff1A2831),
            fontSize: fSize(24),
            fontWeight: FontWeight.w700));
  }

  Widget forgotPwdField() {
    return SizedBox(
        width: wScale(295),
        height: hScale(24),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Row(children: [
            rememberMeCheckBox(),
            rememberMeTitle(),
          ]),
          forgotPwdButton()
        ]));
  }

  Widget rememberMeCheckBox() {
    return SizedBox(
        width: hScale(14),
        height: hScale(14),
        child: Transform.scale(
          scale: 0.7,
          child: Checkbox(
            value: flagRemember,
            activeColor: const Color(0xff30E7A9),
            onChanged: (value) {
              setState(() {
                flagRemember = value!;
              });
            },
          ),
        ));
  }

  Widget rememberMeTitle() {
    return Container(
      margin: EdgeInsets.only(left: wScale(17)),
      child: Text("Remember Me",
          style:
              TextStyle(fontSize: fSize(14), color: const Color(0xFF515151))),
    );
  }

  Widget forgotPwdButton() {
    return TextButton(
      style: TextButton.styleFrom(
        primary: const Color(0xff515151).withOpacity(0.5),
        padding: EdgeInsets.zero,
        textStyle: TextStyle(
            fontSize: fSize(14),
            decoration: TextDecoration.underline,
            color: const Color(0xff515151).withOpacity(0.5)),
      ),
      onPressed: () {
        handleForgotPwd();
      },
      child: const Text('Forgot Password'),
    );
  }

  Widget loginButton() {
    return SizedBox(
        width: wScale(295),
        height: hScale(56),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: const Color(0xff1A2831),
            side: const BorderSide(width: 0, color: Color(0xff1A2831)),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
          ),
          onPressed: () {
            handleLogin();
          },
          child: Text("Login",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: fSize(16),
                  fontWeight: FontWeight.w700)),
        ));
  }

  Widget registerField() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Don't have an account?",
            style:
                TextStyle(color: const Color(0xFF666666), fontSize: fSize(14))),
        registerButton()
      ],
    );
  }

  Widget registerButton() {
    return TextButton(
      style: TextButton.styleFrom(
        primary: const Color(0xff29c490),
        textStyle:
            TextStyle(fontSize: fSize(14), color: const Color(0xff29c490)),
      ),
      onPressed: () {
        handleRegister();
      },
      child: const Text('Register'),
    );
  }
}
